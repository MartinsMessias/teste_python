import heapq


class Orders:
    @staticmethod
    def combine_orders(requests, n_max):
        """
        Calculates the minimum number of trips necessary to deliver all the orders.
        :param requests: list of orders
        :param n_max: maximum number of orders that can be delivered in a single trip
        :return: minimum number of trips necessary to deliver all the orders
        """
        # Transform the list of requests into a min heap desc order
        heap = [-request for request in requests]
        heapq.heapify(heap)

        num_trips = 0  # Num of trips

        # While there are still requests to be delivered
        while heap:
            # Get the largest request to be delivered
            current_request = -heapq.heappop(heap)
            
            # If the next request can be delivered with the current trip
            if heap and current_request + heap[0] <= n_max:
                # Get the next request to be delivered
                next_request = -heapq.heappop(heap)
                num_trips += 1
            else:
                num_trips += 1

        return num_trips


if __name__ == '__main__':
    orders = [70, 30, 10]
    n_max = 100
    expected_orders = 2

    how_many = Orders().combine_orders(orders, n_max)
    assert how_many == expected_orders