import heapq
import concurrent.futures
import os


class Contract:
    def __init__(self, id, debt):
        self.id = id
        self.debt = debt

    def __str__(self):
        return 'id={}, debt={}'.format(self.id, self.debt)


class ContractService:
    def get_top_N_open_contracts(self, open_contracts, renegotiated_contracts, top_n):
        """Returns the top N open contracts, sorted by debt in descending order.
        :param open_contracts: list of open contracts
        :param renegotiated_contracts: list of contract ids that have been renegotiated
        :param top_n: number of top contracts to return
        :return: list of top N open contracts"""

        # Filter out contracts that have been renegotiated and divide the remaining contracts into chunks
        open_contracts = self._filter_open_contracts(open_contracts, renegotiated_contracts)
        num_threads = min(len(open_contracts), os.cpu_count())
        chunks = self._split_contracts_into_chunks(open_contracts, num_threads)

        # Sort each chunk of contracts in parallel and merge the results
        with concurrent.futures.ThreadPoolExecutor(max_workers=num_threads) as executor:
            futures = [executor.submit(self._sort_contracts_by_debt, chunk) for chunk in chunks]
            results = [future.result() for future in futures]

        # Merge the sorted results and return the top N contracts
        sorted_contracts = heapq.merge(*results, key=lambda contract: -contract.debt)
        return [contract.id for contract in sorted_contracts][:top_n]

    def _filter_open_contracts(self, open_contracts, renegotiated_contracts):
        return [contract for contract in open_contracts if contract.id not in renegotiated_contracts]
    
    def _split_contracts_into_chunks(self, contracts, num_chunks):
        chunk_size = len(contracts) // num_chunks
        return [contracts[i:i + chunk_size] for i in range(0, len(contracts), chunk_size)]

    def _sort_contracts_by_debt(self, contracts):
        return sorted(contracts, key=lambda contract: -contract.debt)


if __name__ == '__main__':
    # Test cases
    contracts = [
        Contract(1, 1),
        Contract(2, 2),
        Contract(3, 3),
        Contract(4, 4),
        Contract(5, 5)
    ]
    renegotiated = [3]
    top_n = 3

    contract_service = ContractService()
    actual_open_contracts = contract_service.get_top_N_open_contracts(contracts, renegotiated, top_n)
    expected_open_contracts = [5, 4, 2]
    assert expected_open_contracts == actual_open_contracts